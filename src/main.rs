#![feature(async_closure)]


#[macro_use]
extern crate serde_derive;

mod session;
mod command;

use tokio::time::{self, Duration};
use tokio_tungstenite::tungstenite::Message;
use session::Session;
use std::{env, io::Error, sync::Arc, collections::HashMap};
use command::Command;

use futures_util::{
    future, pin_mut,
    stream::TryStreamExt,
    StreamExt, SinkExt
};
use log::{info, debug};
use std::sync::Mutex;
use tokio::{net::{TcpListener, TcpStream}};

#[tokio::main]
async fn main() -> Result<(), Error> {
    env_logger::init();
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "0.0.0.0:2430".to_string());

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let mut listener = try_socket.expect("Failed to bind");
    info!("Listening on: {}", addr);

    let sessions: Arc<Mutex<HashMap<String, Session>>> = Arc::new(Mutex::new(HashMap::new()));

    while let Ok((stream, _)) = listener.accept().await {
        tokio::spawn(accept_connection(stream, sessions.clone()));
    }

    Ok(())
}

async fn accept_connection(stream: TcpStream, sessions: Arc<Mutex<HashMap<String, Session>>>) {
    println!("{:?}", sessions.lock());
    let addr = stream
        .peer_addr()
        .expect("connected streams should have a peer address");
    info!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(stream)
        .await
        .expect("Error during the websocket handshake occurred");

    info!("New WebSocket connection: {}", addr);

    let (mut write, mut read) = ws_stream.split();
    loop {
        match read.next().await {
            Some(msg) => {
                if let Ok(msg) = msg {
                    if let Ok(msg) = msg.into_text() {
                        // println!("{}", msg);
                            let command: serde_json::Result<Command> = serde_json::from_str(&msg);
                            match command {
                                Ok(command) => {
                                    match command.command.as_str() {
                                        "create-session" => {
                                            info!("create-session");
                                            let session = Session::new();
                                            debug!("{:?}", session);
                                            let _ = write.send(Message::Text(session.session_id.clone())).await;
                                            sessions.lock().unwrap().insert(session.session_id.clone(), session);
                                        },
                                        _ => {}
                                    }
                                },
                                _ => {}
                            }
                        }
                    }
                }
            None => {
                time::delay_for(Duration::from_millis(100)).await;
            }
        }
    }
}