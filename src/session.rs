

use uuid::Uuid;

pub fn generate_session_id() -> String {
    Uuid::new_v4().to_string()
}


#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Session {
    pub session_id: String,
}


impl Session {
    pub fn new() -> Self {
        Session {
            session_id: generate_session_id(),
        }
    }
}



